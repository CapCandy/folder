package com.example.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.junit.Assert;

import java.util.List;

//comment the above line and uncomment below line to use Chrome
//import org.openqa.selenium.chrome.ChromeDriver;
@Component
public class SelTest implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception{
        System.setProperty("webdriver.gecko.driver", "/root/gecko/geckodriver");
	WebDriver driver = new FirefoxDriver();

        String baseUrl = "http://192.168.56.102:1500";

        driver.get(baseUrl);

        WebElement footer = driver.findElement(By.id("block_various_links_footer"));
        if(footer == null ){
            System.out.println("Test failed - footer not found");
            return;
        }
        WebElement aboutUs = null;
        List<WebElement> elements = footer.findElements(By.tagName("li"));

        for(WebElement el : elements){
            if(el.getText().equals("About us")) {
                aboutUs = el;
                break;
            }
        }
        //Thread.sleep(10);
        if(aboutUs != null){
            System.out.println("Test Passed - About Us found");
            aboutUs = aboutUs.findElement(By.tagName("a"));
            aboutUs.click();
        }else{
            System.out.println("Test Failed - About Us not found");
        }
        try {
            WebElement checkPage = driver.findElement(By.xpath("//div[@class='alert alert-danger']"));
            System.out.println("Test Failed - page not found");
            Assert.assertEquals("bla","nope");
        }catch(Exception e){
            Assert.assertEquals("bla","bla");
            System.out.println("Test Passed - page opened");
        }

        //Thread.sleep(10000);

        driver.quit();

    }
}
